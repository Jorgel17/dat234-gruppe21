import re
import requests

datapor = 'https://links.datapor.no'


def get_content(url: str):
    source = requests.get(url)
    content = source.content
    return (content)

def task_1():
    print("Task 1:")
    content = str(get_content(datapor))
    print(content)
    print("--------------------------------")


def task_2():
    print("\nTask 2:")
    content = str(get_content(datapor))
    http = re.findall('http[^s]', content)
    https = re.findall('https', content)
    print("Found", len(http),  "HTTP URL's")
    print("Found", len(https),  "HTTPS URL's")
    print("--------------------------------")


def task_3():
    print("\nTask 3:")
    content = str(get_content(datapor))
    urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
    tlds = []
    for url in urls:
        tld = url.split('//')[1]
        tld = tld.split('/')[0]
        tld = tld.split('.')
        tld.reverse()
        tld = tld[0]
        if tld not in tlds:
            tlds.append(tld)
    print(f"Unique TLDs are {tlds} \n")
    print("--------------------------------")


def task_4():
    print("\nTask 4:")
    content = str(get_content(datapor))
    urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
    hostnames = []
    count = 0
    for url in urls:
        h = url.split('//')[1]
        hostname = h.split('/')[0]
        count += 1
        if hostname not in hostnames:
            hostnames.append(hostname)
    print(f"Unique hostnames are {hostnames} \n")
    print("--------------------------------")


def task_5():
    print("\nTask 5:")
    content = str(get_content(datapor))
    tags = re.findall(r"</?([^ >/]+)", content)
    tagsArr = []
    for tag in tags:
        if tag not in tagsArr and tag != "!DOCTYPE":
            tagsArr.append(tag)
    print(f"Unique HTML tags are {tagsArr} \n")
    print("--------------------------------")


def task_6():
    print("\nTask 6:")
    content = str(get_content(datapor))
    urls = re.findall(r"http[s]?://(?:[a-zA-Z]|[0-9]|[$-_@.&+]|[!*\(\),]|(?:%[0-9a-fA-F][0-9a-fA-F]))+", content)
    head = re.findall(r"<title.*?>(.*?)<\/title>*", content)
    titles = []
    for url in urls:
        print("Inspecting", url)
        contenttemp = str(get_content(url))
        print("got content")
        headtemp = re.search(r"<title.*?>(.*?)<\/title>*", contenttemp)
        if headtemp is not None:
            htmlfilter = re.compile('<.*?>|&([a-z0-9]+|#[0-9]{1,6}|#x[0-9a-f]{1,6});')
            titlestring = re.sub(htmlfilter, '', headtemp.group(0))
            titles.append(titlestring)
            print(titlestring)
    print("Titles found: ")
    for title in titles:
        print(title)
    print("--------------------------------")


if __name__ == "__main__":
    #task_1()
    #task_2()
    #task_3()
    #task_4()
    #task_5()
    task_6()
