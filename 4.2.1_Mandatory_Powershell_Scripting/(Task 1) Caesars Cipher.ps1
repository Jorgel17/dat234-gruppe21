﻿# Decode and encode using Caesar Cipher method

# Function to decrypt the data using Caesar cipher. 
function decrypt([string]$cipher, [int32]$key){
    $decrypted = ""
    
    foreach($letterD in [char[]]$cipher){
        
        $letterDNum = [char]$letterD -as [int]

        $letterENum = $letterDNum - $key

        if ($letterD -cmatch “^[A-Z]*$”){
            if($letterENum -lt 65){
                $letterENum = $letterENum + 26
            }
        } else {
            if($letterENum -lt 97){
                $letterENum = $letterENum + 26
            }
        }

        if($letterD -eq " "){
            [char]$letterE = [char]$letterD
        }else{
            [char]$letterE = [char]$letterENum
        }


        if($letterDNum -in 65..90 -or $letterDNum -in 97..122){
            $decrypted += [char]$letterE
        }else{
            $decrypted += [char]$letterDNum
        }
        
        
    }

    Write-Host $decrypted
}
# Function to encrypt using caesar cipher
function encrypt([string]$text, [int32]$key){
    
    $encrypted = ""
    foreach($letterE in [char[]]$text){
        
        $letterENum = [char]$letterE -as [int]

        if($letterENum -in 65..90 -or $letterENum -in 97..122){
            $letterDNum = $letterENum + $key
        }else{
            $letterDNum = $letterENum
        }

        if ($letterE -cmatch “^[A-Z]*$”){
            if($letterDNum -gt 90){
                $letterDNum = $letterDNum - 26
                
            }
        } else {
            if($letterDNum -gt 122){
                $letterDNum = $letterDNum - 26
            }
        }
        if($letterE -eq " "){
            [char]$letterD = [char]$letterE
        }else{
            [char]$letterD = [char]$letterDNum
        }
        
        $encrypted += [char]$letterD
    }
    Write-Host $encrypted

    
}
# Input from user to use in the encryption/decryption process
$code =  Read-Host -Prompt 'Input String to decipher or encrypt: '
$key = Read-Host -Prompt 'Input the key you would like to use for Caesars Cipher: '
$func =  Read-Host -Prompt 'Input the function you would like to use `e` for encrypt, or `d` for decrypt: '

$keyNum = [int32]$key
#$code = $code.ToLower();

# If test to select function encrypt og decrypt
if([string]$func -eq "d"){
    Write-Host "Decrypting"
    decrypt $code $keyNum

}elseif([string]$func -eq "e"){
    Write-Host "Encrypting"
    encrypt $code $keyNum

}else{
    Write-Host "No valid function found"
}